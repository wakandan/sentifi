package com.sentifi.khoa.ticker;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by akai on 7/3/17.
 */
public class TickerDateTest {
    @Test
    public void getTWAP() throws Exception {
        TickerDate tickerDate = new TickerDate("GE", "2017-01-01", 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0);
        assertEquals(tickerDate.getTWAP(), 1, 0);
        tickerDate = new TickerDate("GE", "2017-01-01", 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        assertEquals(tickerDate.getTWAP(), 0.75, 0);
    }

}