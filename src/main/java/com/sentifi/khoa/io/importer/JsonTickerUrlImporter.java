package com.sentifi.khoa.io.importer;

import com.sentifi.khoa.ticker.TickerDate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.net.URL;
import java.util.List;

/**
 * Created by akai on 7/4/17.
 */
public class JsonTickerUrlImporter extends JsonTickerImporter {
    String url;

    public JsonTickerUrlImporter(String url) {
        this.url = url;
    }

    @Override
    public List<TickerDate> importData() throws Exception {
        String tickerJson = IOUtils.toString(new URL(url));
        JSONObject tickerJsonObject = (JSONObject) JSONValue.parseWithException(tickerJson);
        return parseJson(tickerJsonObject);
    }
}
