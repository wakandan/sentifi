package com.sentifi.khoa.ticker;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by akai on 7/3/17.
 */
public class TickerDate implements Comparable<TickerDate> {
    private String sym;
    private double open, high, low, close;
    private int vol;
    private double exDividend, splitRatio;
    private double adjOpen, adjHigh, adjLow, adjClose;
    private int adjVol;
    private DateTime date;
    private String dateStr;

    public TickerDate(String sym, String date, double open, double high, double low, double close, int vol, double exDividend,
                      double splitRatio, double adjOpen, double adjHigh, double adjLow, double adjClose, int adjVol) {
        this.sym = sym;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.vol = vol;
        this.exDividend = exDividend;
        this.splitRatio = splitRatio;
        this.adjOpen = adjOpen;
        this.adjHigh = adjHigh;
        this.adjLow = adjLow;
        this.adjClose = adjClose;
        this.adjVol = adjVol;
        this.dateStr = date;
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        this.date = formatter.parseDateTime(date);

    }

    public String getSym() {
        return sym;
    }

    public double getOpen() {
        return open;
    }

    public double getHigh() {
        return high;
    }

    public double getLow() {
        return low;
    }

    public int getVol() {
        return vol;
    }

    public String getDateStr() {
        return dateStr;
    }

    public static TickerDate defaultTickerDate(String sym, String date, double open, double high, double low, double close, int vol) {
        return new TickerDate(sym, date, open, high, low, close, vol, 0, 1, open, high, low, close, vol);
    }

    public double getTWAP() {
        return (open + high + low + close) / 4;
    }

    public DateTime getDate() {
        return date;
    }

    public double getClose() {
        return close;
    }

    @Override
    public int compareTo(TickerDate o) {
        return this.date.compareTo(o.getDate());
    }

    @Override
    public String toString() {
        return "TickerDate{" +
                "sym='" + sym + '\'' +
                ", open=" + open +
                ", high=" + high +
                ", low=" + low +
                ", close=" + close +
                ", vol=" + vol +
                '}';
    }
}
