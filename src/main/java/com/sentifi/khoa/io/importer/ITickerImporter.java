package com.sentifi.khoa.io.importer;

import com.sentifi.khoa.ticker.TickerDate;

import java.util.List;

/**
 * Created by akai on 7/4/17.
 */
public interface ITickerImporter {
    public List<TickerDate> importData() throws Exception;
}
