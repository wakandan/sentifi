package com.sentifi.khoa;

import com.sentifi.khoa.ticker.TickerDate;
import com.sentifi.khoa.ticker.TickerRange;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by akai on 7/4/17.
 */
public class SentifiTest {
    public static final String GE = "GE";

    @Test
    public void testConstructJsonOutput() {
        List<TickerDate> tickers = Arrays.asList(new TickerDate[]{
                TickerDate.defaultTickerDate(GE, "2017-01-01", 1, 1, 1, 1, 1)
        });
        TickerRange range = new TickerRange(tickers);
        List<Double> sma50 = Arrays.asList(new Double[]{0.0});
        List<Double> sma200 = Arrays.asList(new Double[]{0.0});
        List<Double> lwma15 = Arrays.asList(new Double[]{0.0});
        List<Double> lwma50 = Arrays.asList(new Double[]{0.0});
        Sentifi sentifi = new Sentifi();
        JSONObject jsonObject = sentifi.constructJsonOutput(range, sma50, sma200, lwma15, lwma50);
        assertTrue(jsonObject.containsKey("Prices"));
        assertTrue(jsonObject.get("Prices") instanceof JSONArray);
        JSONArray array = (JSONArray) jsonObject.get("Prices");
        assertEquals(1, array.size());
        JSONObject ticker = (JSONObject) array.get(0);
        String checkKeys = "Ticker Date Open High Low Close Volume TWAP-Open TWAP-High TWAP-Low TWAP-Close SMA-50 SMA-200 LWMA-15 LWMA-50";
        for (String key : checkKeys.split(" ")) {
            assertEquals(true, ticker.containsKey(key));
        }
    }

    @Test
    public void testConstructCsvRow() {
        Sentifi sentifi = new Sentifi();
        String csvRow = sentifi.constructCsvRow(0,
                TickerDate.defaultTickerDate(GE, "2017-01-01", 1, 1, 1, 1, 1),
                Arrays.asList(new Double[]{0.0}),
                Arrays.asList(new Double[]{0.0}),
                Arrays.asList(new Double[]{0.0}),
                Arrays.asList(new Double[]{0.0})
        );
        assertEquals(14, csvRow.trim().split(",").length);
    }

}