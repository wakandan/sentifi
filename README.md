# README #

Sentifi data engineer technical assignment

### How do I get set up? ###

* run `./gradlew shadowJar` to build & bundle this application to a fat jar. The jar file could be found in `build/libs/`
* to run the app, do `java -jar build/libs/com.khoa.sentifi-1.0-SNAPSHOT-all.jar -h`. This will display the available options
```

usage: Script to calculate TWAP, LWMA values for ticker
 -f <arg>   json file to import from. e.g: -f GE.json
 -h         display this message
 -o <arg>   output file name. e.g: -t GE_out (output GE_out.json and
            GE_out.csv)
 -t <arg>   ticker name. e.g: -t GE
 -u <arg>   json url to import from. e.g: -u https://www.quandl.com/api/v3/datasets/WIKI/GE.json
```
- For `-u` option, user use time filter parameter provided by quandl.com to limit the data set. For example: `https://www.quandl.com/api/v3/datasets/WIKI/FB.json?start_date=2016-01-01&end_date=2017-01-01`
- `./gradlew clean test` to run tests

### Application design considerations ###
- **Premature-optimization is the root of all evil**
- ** KISS **
- There are conflicting information between the [original requirement](https://docs.google.com/document/d/1_nn5JWZO8FLDTDyjLMVUGosoyON-GL671YoXmFkV6kA/edit#) and publicly available guide/wiki online. Since no further question could be asked, I implemented the requirements based on my understanding of required statistic:
    - TWAP: according to [this guide](http://empirica.io/blog/twap-strategy/) the TWAP is calculated as a moving average per time period. However in the requirement it is calculated as the average price of open, high, low and close price over 1 day. That should mean the TWAP value for open, high, low and close price are the same. 
    - SMA: according to [this guide](http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:moving_averages), its example show that for a n-day range of ticker values, the first n-1 day (day 1 to day n-1) will have SMA-n value to be 0, the nth day will have a non-zero SMA-n value. However in the requirement it's said that `N-Day Simple Moving Average is calculated by summing the past N day closing prices and dividin g by N. For the first N days in the series, set the value to 0 when writing to a file.`, meaning that only from day (n+1)th that the SMA-n value is not zero. In the code, the SMA-n value is calculated as the first part.
- The content of json output for the first requirement is ambiguous.
```
 "Prices": {
      "Ticker": "GE",
      "Date": "1999-03-30",
      "Open": "53.23",
      "High": "53.23",
      "Low": "53.23",
      "Close": "53.23",
      "Volume": "53.23",
      "TWAP-Open": "53.23",
      "TWAP-High": "53.23",
      "TWAP-Low": "53.23",
      "TWAP-Close": "53.23"
    }
```
The prices should be an array, otherwise how would the json file contains the list of ticker prices over a period of time

- SMA alert is only calculated for latest day in the sequence
- The json output and csv ouptut could have been moved into their separated classes. However as the specs required always outputing into csv and json, it's simpler to put them both into the main `Sentifi` class and also more efficient as sma & lwma values are only calculated once and reused in both output formats
- The function `defaultTickerDate` was created under the assumption that the adjusted values and the real values are mostly the same. While this assumption may not be correct, it is safe to use because it's only used in tests and it does simplify the boilerplates in test files.
- The ticker sequence is always sorted ascendingly and only sorted once when setting into the range object. This was to prevent the use cases where user could add each individual `TickerDate` object one by one. This case is more complicated to maintain and seems overkill in this context
- The apache cli library was used to provide a convenient way to run the script
- Some of the functionalities are done using well known libraries. Reinventing the wheel was not preferred
