package com.sentifi.khoa.ticker;

/**
 * Created by akai on 7/4/17.
 */
public class AlertSMA {
    protected String status;
    protected TickerDate ticker;
    protected double sma50, sma200;

    public AlertSMA(String status, TickerDate ticker, double sma50, double sma200) {
        this.status = status;
        this.ticker = ticker;
        this.sma50 = sma50;
        this.sma200 = sma200;
    }

    public String getStatus() {
        return status;
    }

    public TickerDate getTicker() {
        return ticker;
    }

    public double getSma50() {
        return sma50;
    }

    public double getSma200() {
        return sma200;
    }

    @Override
    public String toString() {
        return "AlertSMA{" +
                "status='" + status + '\'' +
                ", ticker=" + ticker +
                ", sma50=" + sma50 +
                ", sma200=" + sma200 +
                '}';
    }
}
