package com.sentifi.khoa.ticker;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by akai on 7/3/17.
 */
public class TickerRange {
    List<TickerDate> tickers;
    DateTime startDate, endDate;

    public TickerRange() {
        tickers = new ArrayList<>();
    }

    /**
     * Reorder tickers if passed into constructor
     *
     * @param tickers
     */
    public TickerRange(List<TickerDate> tickers) {
        if (tickers == null || tickers.size() < 1) {
            this.tickers = new ArrayList<>();
        } else {
            this.tickers = tickers;
            Collections.sort(tickers);
            this.startDate = tickers.get(0).getDate();
            this.endDate = tickers.get(tickers.size() - 1).getDate();
        }
    }

    /**
     * Get simple moving average price within n days
     *
     * @param days
     * @return List<Double>
     */
    public List<Double> calcSMA(int days) {
        List<Double> result = new ArrayList<>();
        //invalid case
        if (tickers.size() < 1 || days <= 0)
            return result;
        //# of days > sequence size
        if (days > tickers.size()) {
            for (TickerDate ticker : tickers) {
                result.add(0.0);
            }
            return result;
        }
        double total = 0;
        int head = days - 1;
        int tail = 0;
        int i = 0;
        while (i < days - 1) {
            result.add(0.0);
            total += tickers.get(i).getClose();
            i += 1;
        }

        while (head < tickers.size()) {
            total += tickers.get(head).getClose();
            result.add(total / days);
            total -= tickers.get(tail).getClose();
            tail += 1;
            head += 1;
        }

        return result;
    }

    /**
     * Get linear weighted moving average prices
     *
     * @param days
     * @return List<Double>
     */
    public List<Double> calcLWMA(int days) {
        List<Double> result = new ArrayList<>();
        if (tickers.size() < 1 || days <= 0) {
            return result;
        }
        for (int i = 0; i < tickers.size(); i++) {
            int day = days;
            double total = 0;
            int totalDays = 0;
            for (int j = i; j >= 0 && j >= i - days + 1; j--) {
                total += tickers.get(j).getClose() * day;
                totalDays += day;
                day -= 1;
            }
            result.add(total / totalDays);
        }
        return result;
    }

    /**
     * Get ticker at index
     *
     * @param index
     * @return
     */
    public TickerDate get(int index) {
        if (index < 0 || index >= tickers.size())
            return null;
        return tickers.get(index);
    }

    public int size() {
        return tickers.size();
    }

    /**
     * if sma50 < sma200 => return "bearish"
     * else if sma50 > sma200 && vol > 1.1*avg_vol(50) => return "bullish"
     * else return null
     *
     * @return
     */
    public AlertSMA alert(int small, int big) {
        if (tickers.size() < big)
            return null;
        List<Double> smaSmallList = calcSMA(small);
        List<Double> smaBigList = calcSMA(big);
        if (smaSmallList.size() > 0 && smaBigList.size() > 0) {
            TickerDate ticker = tickers.get(tickers.size() - 1);
            double smaSmall = smaSmallList.get(smaSmallList.size() - 1);
            double smaBig = smaBigList.get(smaBigList.size() - 1);
            if (smaSmall < smaBig) {
                return new AlertSMA("bearish", ticker, smaSmall, smaBig);
            } else if (smaSmall > smaBig) {
                double totalVolume = 0;
                for (int i = tickers.size() - 1; i >= small; i--) {
                    totalVolume += tickers.get(i).getVol();
                }
                if (totalVolume / small * 1.1 < ticker.getVol()) {
                    return new AlertSMA("bullish", ticker, smaSmall, smaBig);
                }
            }
        }

        return null;
    }
}
