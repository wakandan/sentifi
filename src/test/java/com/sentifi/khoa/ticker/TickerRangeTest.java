package com.sentifi.khoa.ticker;

import com.sentifi.khoa.Sentifi;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by akai on 7/3/17.
 */
public class TickerRangeTest {

    public static final String GE = "GE";

    @Test
    public void setTickerListNull() {
        TickerRange range = new TickerRange(null);
        assertEquals(0, range.size());
    }

    @Test
    public void setTickerListEmpty() {
        TickerRange range = new TickerRange(new ArrayList<>());
        assertEquals(0, range.size());
    }

    @Test
    public void setTickerListValid() {
        TickerDate[] tickerDates = new TickerDate[]{
                TickerDate.defaultTickerDate(GE, "2017-01-06", 1, 1, 1, 6, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-01", 1, 1, 1, 1, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-02", 1, 1, 1, 2, 1),
        };
        TickerRange range = new TickerRange(Arrays.asList(tickerDates));
        assertEquals(3, range.size());
        assertEquals("2017-01-01", range.get(0).getDateStr());
        assertEquals("2017-01-02", range.get(1).getDateStr());
        assertEquals("2017-01-06", range.get(2).getDateStr());
    }

    @Test
    public void calcSMAEmptyTickers() throws Exception {
        TickerRange range = new TickerRange();
        List<Double> sma = range.calcSMA(10);
        assertNotNull(sma);
        assertEquals(0, sma.size());
    }

    @Test
    public void calcSMALongerDays() {
        TickerDate[] tickerDates = new TickerDate[]{
                TickerDate.defaultTickerDate(GE, "2017-01-01", 1, 1, 1, 1, 1)
        };
        List<TickerDate> tickers = Arrays.asList(tickerDates);
        TickerRange range = new TickerRange(tickers);
        List<Double> sma = range.calcSMA(10);
        assertNotNull(sma);
        assertEquals(1, sma.size());
        assertEquals(0, (double) sma.get(0), 0);
    }

    @Test
    public void calcSMAValid() {
        TickerDate[] tickerDates = new TickerDate[]{
                TickerDate.defaultTickerDate(GE, "2017-01-01", 1, 1, 1, 1, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-02", 1, 1, 1, 2, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-06", 1, 1, 1, 6, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-03", 1, 1, 1, 3, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-04", 1, 1, 1, 4, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-05", 1, 1, 1, 5, 1),
        };
        List<TickerDate> tickers = Arrays.asList(tickerDates);
        TickerRange range = new TickerRange(tickers);
        List<Double> sma = range.calcSMA(3);
        assertNotNull(sma);
        assertEquals(6, sma.size());
        assertEquals(0, sma.get(0), 0);
        assertEquals(0, sma.get(1), 0);
        assertEquals(2, sma.get(2), 0.01);
        assertEquals(3, sma.get(3), 0.01);
        assertEquals(4, sma.get(4), 0.01);
        assertEquals(5, sma.get(5), 0.01);
    }

    @Test
    public void calcLWMAEmptyTickers() {
        TickerRange range = new TickerRange();
        List<Double> lwma = range.calcLWMA(10);
        assertNotNull(lwma);
        assertEquals(0, lwma.size());
    }

    @Test
    public void calcLWMAValid() {
        TickerDate[] tickerDates = new TickerDate[]{
                TickerDate.defaultTickerDate(GE, "2017-01-01", 1, 1, 1, 1, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-02", 1, 1, 1, 2, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-06", 1, 1, 1, 6, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-03", 1, 1, 1, 3, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-04", 1, 1, 1, 4, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-05", 1, 1, 1, 5, 1),
        };
        List<TickerDate> tickers = Arrays.asList(tickerDates);
        TickerRange range = new TickerRange(tickers);
        List<Double> lwma = range.calcLWMA(3);
        assertNotNull(lwma);
        assertEquals(6, lwma.size());
        assertEquals(1, lwma.get(0), 0.01);
        assertEquals(8.0 / 5, lwma.get(1), 0.01);
        assertEquals((3 * 3 + 2 * 2 + 1) * 1.0 / 6, lwma.get(2), 0.01);
        assertEquals((3 * 4 + 2 * 3 + 1 * 2) * 1.0 / 6, lwma.get(3), 0.01);
        assertEquals((3 * 5 + 2 * 4 + 1 * 3) * 1.0 / 6, lwma.get(4), 0.01);
        assertEquals((3 * 6 + 2 * 5 + 1 * 4) * 1.0 / 6, lwma.get(5), 0.01);
    }

    @Test
    public void testAlertSMAInvalid() {
        TickerRange range = new TickerRange();
        AlertSMA alert = range.alert(50, 100);
        assertNull(alert);
    }

    @Test
    public void testAlertSMABullish() {
        TickerDate[] tickerDates = new TickerDate[]{
                TickerDate.defaultTickerDate(GE, "2017-01-01", 1, 1, 1, 1, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-02", 1, 1, 1, 2, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-06", 1, 1, 1, 6, 10),
                TickerDate.defaultTickerDate(GE, "2017-01-03", 1, 1, 1, 3, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-04", 1, 1, 1, 4, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-05", 1, 1, 1, 5, 1),
        };
        List<TickerDate> tickers = Arrays.asList(tickerDates);
        TickerRange range = new TickerRange(tickers);
        AlertSMA alert = range.alert(2, 4);
        assertNotNull(alert);
        assertEquals("bullish", alert.getStatus());
    }

    @Test
    public void testAlertSMABearish() {
        TickerDate[] tickerDates = new TickerDate[]{
                TickerDate.defaultTickerDate(GE, "2017-01-01", 1, 1, 1, 1, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-02", 1, 1, 1, 2, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-06", 1, 1, 1, 6, 10),
                TickerDate.defaultTickerDate(GE, "2017-01-03", 1, 1, 1, 100, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-04", 1, 1, 1, 4, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-05", 1, 1, 1, 5, 1),
        };
        List<TickerDate> tickers = Arrays.asList(tickerDates);
        TickerRange range = new TickerRange(tickers);
        AlertSMA alert = range.alert(2, 4);
        assertNotNull(alert);
        assertEquals("bearish", alert.getStatus());
    }

    @Test
    public void testAlertSMANoChange() {
        TickerDate[] tickerDates = new TickerDate[]{
                TickerDate.defaultTickerDate(GE, "2017-01-01", 1, 1, 1, 1, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-02", 1, 1, 1, 1, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-06", 1, 1, 1, 1, 10),
                TickerDate.defaultTickerDate(GE, "2017-01-03", 1, 1, 1, 1, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-04", 1, 1, 1, 1, 1),
                TickerDate.defaultTickerDate(GE, "2017-01-05", 1, 1, 1, 1, 1),
        };
        List<TickerDate> tickers = Arrays.asList(tickerDates);
        TickerRange range = new TickerRange(tickers);
        AlertSMA alert = range.alert(2, 4);
        assertNull(alert);
    }
}