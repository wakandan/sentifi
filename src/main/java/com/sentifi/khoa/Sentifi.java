package com.sentifi.khoa;

import com.sentifi.khoa.io.importer.ITickerImporter;
import com.sentifi.khoa.io.importer.JsonTickerFileImporter;
import com.sentifi.khoa.io.importer.JsonTickerUrlImporter;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.sentifi.khoa.ticker.AlertSMA;
import com.sentifi.khoa.ticker.TickerDate;
import com.sentifi.khoa.ticker.TickerRange;

import java.io.*;
import java.util.List;

public class Sentifi {
    public static final String OPT_OUTPUT_FILENAME = "o";
    public static final String OPT_TICKER = "t";
    public static final String OPT_HELP = "h";
    public static final String OPT_INPUT_FILE = "f";
    public static final String OPT_INPUT_URL = "u";

    private static Logger logger = Logger.getLogger(Sentifi.class.getName());

    /**
     * Construct a row of output csv ticker file
     *
     * @param index
     * @param ticker
     * @param sma50
     * @param sma200
     * @param lwma15
     * @param lwma50
     * @return
     */
    protected String constructCsvRow(
            int index,
            TickerDate ticker,
            List<Double> sma50,
            List<Double> sma200,
            List<Double> lwma15,
            List<Double> lwma50
    ) {
        StringBuilder sb = new StringBuilder();
        sb.append(ticker.getSym() + ", ");
        sb.append(ticker.getDateStr() + ", ");
        sb.append(ticker.getOpen() + ", ");
        sb.append(ticker.getHigh() + ", ");
        sb.append(ticker.getLow() + ", ");
        sb.append(ticker.getClose() + ", ");
        sb.append(ticker.getTWAP() + ", ");
        sb.append(ticker.getTWAP() + ", ");
        sb.append(ticker.getTWAP() + ", ");
        sb.append(ticker.getTWAP() + ", ");
        sb.append(sma50.get(index) + ", ");
        sb.append(sma200.get(index) + ", ");
        sb.append(lwma15.get(index) + ", ");
        sb.append(lwma50.get(index));
        sb.append("\n");
        return sb.toString();
    }

    /**
     * Output ticker data to csv
     *
     * @param range
     * @param sma50
     * @param sma200
     * @param lwma15
     * @param lwma50
     */
    protected void outputCsv(
            TickerRange range,
            List<Double> sma50,
            List<Double> sma200,
            List<Double> lwma15,
            List<Double> lwma50
    ) throws IOException {
        logger.info("Writing csv file");
        //writing to csv file
        BufferedWriter bw = new BufferedWriter(new FileWriter(String.format("%s.csv", outputFilename)));
        for (int i = 0; i < range.size(); i++) {
            TickerDate ticker = range.get(i);
            String tickerRow = constructCsvRow(i, ticker, sma50, sma200, lwma15, lwma50);
            bw.write(tickerRow);
        }
        bw.close();
        logger.info("Done writing csv");
    }

    /**
     * Construct json object from ticker data & indicator stats
     *
     * @param range
     * @param sma50
     * @param sma200
     * @param lwma15
     * @param lwma50
     * @return
     */
    protected JSONObject constructJsonOutput(
            TickerRange range,
            List<Double> sma50,
            List<Double> sma200,
            List<Double> lwma15,
            List<Double> lwma50
    ) {
        JSONObject json = new JSONObject();
        JSONArray pricesJson = new JSONArray();
        json.put("Prices", pricesJson);
        for (int i = 0; i < range.size(); i++) {
            TickerDate ticker = range.get(i);
            JSONObject tickerJson = new JSONObject();
            tickerJson.put("Ticker", ticker.getSym());
            tickerJson.put("Date", ticker.getDateStr());
            tickerJson.put("Open", ticker.getOpen());
            tickerJson.put("High", ticker.getHigh());
            tickerJson.put("Low", ticker.getLow());
            tickerJson.put("Close", ticker.getClose());
            tickerJson.put("Volume", ticker.getVol());
            tickerJson.put("TWAP-Open", ticker.getTWAP());
            tickerJson.put("TWAP-High", ticker.getTWAP());
            tickerJson.put("TWAP-Low", ticker.getTWAP());
            tickerJson.put("TWAP-Close", ticker.getTWAP());
            tickerJson.put("SMA-50", sma50.get(i));
            tickerJson.put("SMA-200", sma200.get(i));
            tickerJson.put("LWMA-15", lwma15.get(i));
            tickerJson.put("LWMA-50", lwma50.get(i));
            pricesJson.add(tickerJson);
        }
        return json;
    }

    /**
     * Output ticker data to json
     *
     * @param range
     * @param sma50
     * @param sma200
     * @param lwma15
     * @param lwma50
     * @throws IOException
     */
    protected void outputJson(
            TickerRange range,
            List<Double> sma50,
            List<Double> sma200,
            List<Double> lwma15,
            List<Double> lwma50
    ) throws IOException {
        logger.info("Writing json file");
        //writing to json file
        BufferedWriter bwJson = new BufferedWriter(new FileWriter(String.format("%s.json", outputFilename)));
        JSONObject json = constructJsonOutput(range, sma50, sma200, lwma15, lwma50);
        bwJson.write(json.toJSONString());
        bwJson.close();
        logger.info("Done writing json file");
    }

    protected void outputAlert(TickerRange range) throws IOException {
        AlertSMA alert = range.alert(4, 5);
        if (alert != null) {
            logger.info("Writing alert");
            logger.debug("Alert is " + alert.toString());
            BufferedWriter alertWriter = new BufferedWriter(new FileWriter("alerts.dat", true));
            StringBuilder alertSb = new StringBuilder();
            alertSb.append(alert.getStatus() + ", ");
            alertSb.append(alert.getTicker().getSym() + ", ");
            alertSb.append(alert.getTicker().getDateStr() + ", ");
            alertSb.append(alert.getTicker().getOpen() + ", ");
            alertSb.append(alert.getTicker().getHigh() + ", ");
            alertSb.append(alert.getTicker().getLow() + ", ");
            alertSb.append(alert.getTicker().getClose() + ", ");
            alertSb.append(alert.getTicker().getVol() + ", ");
            alertSb.append(alert.getSma50() + ", ");
            alertSb.append(alert.getSma200());
            alertSb.append("\n");
            alertWriter.write(alertSb.toString());
            alertWriter.close();
            logger.info("Done writing alert");
        } else {
            logger.debug("No alert found, skipping");
        }
    }

    private void processTicker() {
        logger.info("Reading json file");
        List<TickerDate> tickers = null;
        try {
            tickers = tickerImporter.importData();
        } catch (Exception e) {
            logger.error("Failed to import data");
            e.printStackTrace();
            System.exit(1);
        }
        logger.info(String.format("Read %d ticker", tickers.size()));
        TickerRange range = new TickerRange(tickers);
        List<Double> sma50 = range.calcSMA(50);
        List<Double> sma200 = range.calcSMA(200);
        List<Double> lwma15 = range.calcLWMA(15);
        List<Double> lwma50 = range.calcLWMA(50);

        try {
            outputJson(range, sma50, sma200, lwma15, lwma50);
        } catch (IOException e) {
            logger.error("Failed to output json");
            e.printStackTrace();
        }

        try {
            outputCsv(range, sma50, sma200, lwma15, lwma50);
        } catch (IOException e) {
            logger.error("Failed to output csv");
            e.printStackTrace();
        }

        try {
            outputAlert(range);
        } catch (IOException e) {
            logger.error("Failed to output alert");
            e.printStackTrace();
        }

        logger.info("Done");
    }

    public static void main(String args[]) throws Exception {
        Sentifi sentifi = new Sentifi();
        Options options = new Options();
        options.addOption(OPT_HELP, false, "display this message");
        options.addOption(OPT_TICKER, true, "ticker name. e.g: -t GE");
        options.addOption(OPT_INPUT_FILE, true, "json file to import from. e.g: -f GE.json");
        options.addOption(OPT_INPUT_URL, true, "json url to import from. e.g: -u https://www.quandl.com/api/v3/datasets/WIKI/GE.json");
        options.addOption(OPT_OUTPUT_FILENAME, true, "output file name. e.g: -t GE_out (output GE_out.json and GE_out.csv)");
        HelpFormatter helpFormatter = new HelpFormatter();
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);
        if (commandLine.hasOption(OPT_HELP)) {
            helpFormatter.printHelp("Script to calculate TWAP, LWMA values for ticker", options);
            System.exit(0);
        } else {
            if (!commandLine.hasOption(OPT_TICKER)) {
                helpFormatter.printHelp(String.format("Ticker symbol is required. e.g: -%s GE", OPT_TICKER), options);
                System.exit(1);
            } else {
                sentifi.tickerSym = commandLine.getOptionValue(OPT_TICKER);
            }
            sentifi.outputFilename = sentifi.tickerSym + "_out";
            if (commandLine.hasOption(OPT_OUTPUT_FILENAME)) {
                sentifi.outputFilename = commandLine.getOptionValue(OPT_OUTPUT_FILENAME);
            }
            if (!commandLine.hasOption(OPT_INPUT_FILE) && !commandLine.hasOption(OPT_INPUT_URL)) {
                helpFormatter.printHelp("require either input file name or input file url", options);
                System.exit(1);
            } else {
                if (commandLine.hasOption(OPT_INPUT_FILE)) {
                    sentifi.tickerImporter = new JsonTickerFileImporter(commandLine.getOptionValue(OPT_INPUT_FILE));
                } else if (commandLine.hasOption(OPT_INPUT_URL)) {
                    sentifi.tickerImporter = new JsonTickerUrlImporter(commandLine.getOptionValue(OPT_INPUT_URL));
                }
            }
            sentifi.processTicker();
        }

    }

    private ITickerImporter tickerImporter;
    private String tickerSym;
    private String outputFilename;
}
