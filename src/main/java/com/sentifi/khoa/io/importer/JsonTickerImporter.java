package com.sentifi.khoa.io.importer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.sentifi.khoa.ticker.TickerDate;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by akai on 7/3/17.
 */
public abstract class JsonTickerImporter implements ITickerImporter {
    protected List<TickerDate> parseJson(JSONObject jsonObject) throws Exception {
        List<TickerDate> tickers = new ArrayList<>();
        JSONObject dataSet = (JSONObject) jsonObject.get("dataset");
        JSONArray data = (JSONArray) dataSet.get("data");
        String sym = (String) dataSet.get("dataset_code");
        for (Object obj : data) {
            JSONArray t = (JSONArray) obj;
            String date = (String) t.get(0);
            double open, high, low, close, adjOpen, adjHigh, adjLow, adjClose;
            int vol, adjVol;
            double exDividend, splitRatio;
            open = (double) t.get(1);
            high = (double) t.get(2);
            low = (double) t.get(3);
            close = (double) t.get(4);
            vol = (int) ((double) t.get(5));
            exDividend = (double) t.get(6);
            splitRatio = (double) t.get(7);
            adjOpen = (double) t.get(8);
            adjHigh = (double) t.get(8);
            adjLow = (double) t.get(8);
            adjClose = (double) t.get(8);
            adjVol = (int) ((double) t.get(8));
            TickerDate tickerDate = new TickerDate(sym, date, open, high, low, close, vol, exDividend, splitRatio,
                    adjOpen, adjHigh, adjLow, adjClose, adjVol);
            tickers.add(tickerDate);
        }
        return tickers;
    }
}
