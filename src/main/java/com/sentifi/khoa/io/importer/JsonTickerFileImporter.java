package com.sentifi.khoa.io.importer;

import com.sentifi.khoa.ticker.TickerDate;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.List;

/**
 * Created by akai on 7/4/17.
 */
public class JsonTickerFileImporter extends JsonTickerImporter {
    public JsonTickerFileImporter(String fileName) {
        this.fileName = fileName;
    }

    String fileName;

    @Override
    public List<TickerDate> importData() throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(this.fileName));
        return parseJson(jsonObject);
    }
}
